FROM openjdk:15.0.1
COPY build/libs/*.jar build/libs/demo-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","build/libs/demo-0.0.1-SNAPSHOT.jar"]
